#include <iostream>
#include <string>

class Actor
{

public:
    Actor(std::string name) : currentSlapped_( false ), name_(name) {};

    virtual ~Actor() {};
    virtual void update() = 0;

    void reset() { currentSlapped_ = false; }
    void slap() { nextSlapped_ = true;  }
    bool wasSlapped () {return currentSlapped_;  }

    virtual std::string getName()
    {
        return name_;
    }

    void swap()
    {
        currentSlapped_ = nextSlapped_; 

        nextSlapped_ = false; 

    }

private:
    bool nextSlapped_, currentSlapped_;
    std::string name_;
};

class Stage
{
public:
    void add( Actor * actor, int index )
    {
        actors_[index] = actor;
    }

    void update()
    {
        for ( int i = 0; i < NUM_ACTORS; ++i )
        {
            actors_[i]->update();
        } 
        for ( int i = 0; i < NUM_ACTORS; ++i )
        {
            actors_[i]->swap();
        }
        std::cout << " stage update end \n" << std::endl;
    }

private:
    static const int NUM_ACTORS = 3;
    Actor * actors_[NUM_ACTORS];
};

class Comedian : public Actor
{
public:
    Comedian( std::string name ): Actor (name)  {}
    void face (Actor * actor) { 
        facing_ = actor;

    }
    virtual void update()
    {
        std::cout << " Update Actor name " << getName() << std::endl;
        if ( wasSlapped() )
        {
            std::cout << getName() << " was slapped, He will slapp " << facing_->getName() << std::endl;
            facing_->slap();
        }

    }

private:
     Actor * facing_;
};


int main()
{
    Stage stage;
    Comedian * harry = new Comedian("harry");
    Comedian * baldy = new Comedian("baldy");
    Comedian * chunk = new Comedian("chunk");

    harry->face( baldy );
    baldy->face( chunk );
    chunk->face( harry );

    stage.add( baldy, 1 );
    stage.add( harry, 0 );
    stage.add( chunk, 2 );

    harry->slap();
    stage.update();

    stage.update();
    stage.update();
    stage.update();







    system( "pause" );
    return 0; 
}
